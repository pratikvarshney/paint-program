/* DATED: 16/11/2012 Paint Program By Pratik Varshney */
//NAME                 : Pratik Varshney
//ENROLLMENT NUMBER    : GE0004
//FACULTY NUMBER       : 11PEB002
//CLASS ROLL NUMBER    : A2PE-1
//I wrote this program in Notepad++ AND compiled with Command Prompt
//Paint Program to draw basic shapes (line , rectangle , ellipse , square , circle , etc) and free hand drawing.
//Select arcWidth and arcHeight of RoundedRectangle.
//User can select Stroke Size , line color and fill color.
//UNDO , REDO and CLEAR_ALL options are also available.
//ABOUT dialog box contains My Details.
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.*;
class Title
{
	String title;
	String version;
	String author;
	Title()
	{
		title = "MyPaint";
		version = "v1.0";
		author = "Pratik Varshney";
	}
}
abstract class Detail
{
	String name;
	String enroll_No;
	String faculty_No;
	String class_Roll_No;
	String university;
	abstract public void display();
	public void author()
	{
		final String auth_name = "Pratik Varshney";
		final String branch = "II Year COMPUTER ENGG.";
		enroll_No = "GE0004";
		faculty_No = "11PEB002";
		class_Roll_No = "A2PE-1";
		final String college = "Z.H.C.E.T.";
		final String university = "A.M.U. Aligarh";
		final String country = "INDIA";
		Title t = new Title();
		String abt = "     *****  About "+t.title+" "+t.version+"  *****     ";
		String sepr = "\n--------------------------------------------------\n";
		try
		{
			JOptionPane.showMessageDialog(null,abt+sepr+String.format("%s : %s\n%s : %s\n%s : %s\n%s : %s\n%s : %s\n%s : %s\n%s : %s\n%s : %s","AUTHOR NAME",auth_name,"BRANCH",branch,"ENROLLMENT NUMBER",enroll_No,"FACULTY NUMBER",faculty_No,"CLASS ROLL NUMBER",class_Roll_No,"COLLEGE NAME",college,"UNIVERSITY",university,"COUNTRY",country)+sepr);
		}
		catch(Exception err)
		{
			System.out.println(err);
			System.out.println("!! ERROR in displaying AUTHOR Info !!");
		}
	}
}
class MyInfo extends Detail
{
	MyInfo()
	{
		name = "Pratik Varshney";
		enroll_No = "GE0004";
		faculty_No = "11PEB002";
		class_Roll_No = "A2PE-1";
		university = "A.M.U. Aligarh";
	}
	MyInfo(String n,String e,String f,String c,String u)
	{
		name = n;
		enroll_No = e;
		faculty_No = f;
		class_Roll_No = c;
		university = u;
	}
	public void display()
	{
		try
		{
			System.out.printf("\n%-20s : %s","NAME",name);
			System.out.printf("\n%-20s : %s","ENROLLMENT NUMBER",enroll_No);
			System.out.printf("\n%-20s : %s","FACULTY NUMBER",faculty_No);
			System.out.printf("\n%-20s : %s","CLASS ROLL NUMBER",class_Roll_No);
			System.out.printf("\n%-20s : %s","UNIVERSITY",university);
			System.out.println();
		}
		catch(Exception err)
		{
			System.out.println(err);
			System.out.println("!! ERROR in printing MyInfo !!");
		}
	}
}
public class Paint extends JFrame
{
	ArrayList<Shape> shapes = new ArrayList<Shape>(); //to store shapes
	ArrayList<Color> fill_c = new ArrayList<Color>();
	ArrayList<Color> line_c = new ArrayList<Color>();
	ArrayList<BasicStroke> strk = new ArrayList<BasicStroke>();
	ArrayList<Shape> re_shapes = new ArrayList<Shape>(); //for redo operations
	ArrayList<Color> re_fill_c = new ArrayList<Color>();
	ArrayList<Color> re_line_c = new ArrayList<Color>();
	ArrayList<BasicStroke> re_strk = new ArrayList<BasicStroke>();
	int stype=0;
	BasicStroke sh_stroke = new BasicStroke(2);
	int dotSize = 2;
	int arcWidth = 25;
	int arcHeight = 25;
	int redo_flag = 0;
	JLabel label = new JLabel("Selected Type : FreeHand");
	JLabel label_dot_size = new JLabel("Stroke Size :");
	JLabel label_arc_wd = new JLabel("Arc WIDTH :");
	JLabel label_arc_ht = new JLabel("Arc HEIGHT :");
	Color line_color = Color.BLACK;
	Color fill_color = new Color(255,255,255,0);
	JComboBox dot_size;
	JComboBox arc_width;
	JComboBox arc_height;
	final JButton undo_btn = new JButton("UNDO");
	final JButton redo_btn = new JButton("REDO");
	final JButton about = new JButton("** About **");
	static MyInfo details = new MyInfo();
	public static void main(String[] arr)
	{
		details.display();
		new Paint();
	}
	public Paint()
	{
		Title t = new Title();
		setSize(640,480);
		setTitle(t.title + " " + t.version + " BY " + t.author);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(new PaintArea(), BorderLayout.CENTER);
		setVisible(true);
		JPanel buttonJPanel = new JPanel();
		add( label, BorderLayout.SOUTH );
		buttonJPanel.setLayout( new GridLayout( 19, 1 ) );
		JButton freehand = new JButton("FreeHand");
		JButton line = new JButton("Line");
		JButton rect = new JButton("Rectangle");
		JButton roundrect = new JButton("RoundedRectangle");
		JButton square = new JButton("Square");
		JButton ellipse = new JButton("Ellipse");
		JButton circ = new JButton("Circle");
		JButton changeLineColorJButton = new JButton( "Change line Color" );
		JButton changeFillColorJButton = new JButton( "Change fill Color" );
		JButton clear_btn = new JButton("CLEAR ALL");
		buttonJPanel.add(freehand);
		buttonJPanel.add(line);
		buttonJPanel.add(rect);
		buttonJPanel.add(roundrect);
		buttonJPanel.add(square);
		buttonJPanel.add(ellipse);
		buttonJPanel.add(circ);
		buttonJPanel.add(changeLineColorJButton);
		changeLineColorJButton.addActionListener(new ActionListener() // anonymous inner class
		{
			// display JColorChooser when user clicks button
			public void actionPerformed( ActionEvent event )
			{
				try
				{
					Color col_new = JColorChooser.showDialog(null, "Choose a color", line_color );
					if ( col_new != null )
					line_color = col_new;
				}
				catch(Exception err)
				{
					System.out.println(err);
				}
			} // end method actionPerformed
		} // end anonymous inner class
		);
		buttonJPanel.add(changeFillColorJButton);
		changeFillColorJButton.addActionListener(new ActionListener() // anonymous inner class
		{
			// display JColorChooser when user clicks button
			public void actionPerformed( ActionEvent event )
			{
				try
				{
					Color col_new = JColorChooser.showDialog(null, "Choose a color", fill_color );
					if ( col_new != null )
					fill_color = col_new;
				}
				catch(Exception err)
				{
					System.out.println(err);
				}
			} // end method actionPerformed
		} // end anonymous inner class
		);
		add( buttonJPanel, BorderLayout.WEST );
		buttonJPanel.add(label_dot_size);
		final String[] pen_sizes = {"2","4","6","8","10"};
		final String[] widths_arc = {"25","50","75","100"};
		final String[] heights_arc = {"25","50","75","100"};
		dot_size = new JComboBox<String>(pen_sizes);
		arc_width = new JComboBox<String>(widths_arc);
		arc_height = new JComboBox<String>(heights_arc);
		dot_size.setMaximumRowCount( 3 );
		arc_width.setMaximumRowCount( 3 );
		arc_height.setMaximumRowCount( 3 );
		buttonJPanel.add(dot_size);
		dot_size.addItemListener(
		new ItemListener() // anonymous inner class
		{
			// handle JComboBox event
			public void itemStateChanged( ItemEvent event )
			{
				try
				{
					// determine whether item selected
					if ( event.getStateChange() == ItemEvent.SELECTED )
					{
						dotSize = Integer.parseInt(pen_sizes[dot_size.getSelectedIndex()]);
						sh_stroke = new BasicStroke(dotSize);
					}
				}
				catch(Exception err)
				{
					System.out.println(err);
				}
			} // end method itemStateChanged
		} // end anonymous inner class
		); // end call to addItemListener
		buttonJPanel.add(label_arc_wd);
		buttonJPanel.add(arc_width);
		arc_width.addItemListener(
		new ItemListener() // anonymous inner class
		{
			// handle JComboBox event
			public void itemStateChanged( ItemEvent event )
			{
				try
				{
					// determine whether item selected
					if ( event.getStateChange() == ItemEvent.SELECTED )
					arcWidth = Integer.parseInt(widths_arc[arc_width.getSelectedIndex()]);
				}
				catch(Exception err)
				{
					System.out.println(err);
				}
			} // end method itemStateChanged
		} // end anonymous inner class
		); // end call to addItemListener
		buttonJPanel.add(label_arc_ht);
		buttonJPanel.add(arc_height);
		arc_height.addItemListener(
		new ItemListener() // anonymous inner class
		{
			// handle JComboBox event
			public void itemStateChanged( ItemEvent event )
			{
				try
				{
					// determine whether item selected
					if ( event.getStateChange() == ItemEvent.SELECTED )
					arcHeight = Integer.parseInt(heights_arc[arc_height.getSelectedIndex()]);
				}
				catch(Exception err)
				{
					System.out.println(err);
				}
			} // end method itemStateChanged
		} // end anonymous inner class
		); // end call to addItemListener
		buttonJPanel.add(undo_btn);
		undo_btn.addActionListener(new ActionListener() // anonymous inner class
		{
			public void actionPerformed( ActionEvent event )
			{
				try
				{
					int i = shapes.size()-1;
					if(i>=0)
					{
						re_shapes.add(shapes.get(i));
						re_line_c.add(line_c.get(i));
						re_fill_c.add(fill_c.get(i));
						re_strk.add(strk.get(i));
						shapes.remove(i);
						line_c.remove(i);
						fill_c.remove(i);
						strk.remove(i);
						redo_flag = i;
						redo_btn.setEnabled(true);
						if(i==0)
						{
							undo_btn.setEnabled(false);
						}
						repaint();
					}
				}
				catch(Exception err)
				{
					System.out.println(err);
				}
			} // end method actionPerformed
		} // end anonymous inner class
		);
		buttonJPanel.add(redo_btn);
		redo_btn.addActionListener(new ActionListener() // anonymous inner class
		{
			public void actionPerformed( ActionEvent event )
			{
				try
				{
					int i = re_shapes.size()-1;
					if(i>=0 && redo_flag == shapes.size())
					{
						shapes.add(re_shapes.get(i));
						line_c.add(re_line_c.get(i));
						fill_c.add(re_fill_c.get(i));
						strk.add(re_strk.get(i));
						re_shapes.remove(i);
						re_line_c.remove(i);
						re_fill_c.remove(i);
						re_strk.remove(i);
						redo_flag++;
						if(i==0)
						{
							redo_btn.setEnabled(false);
						}
						undo_btn.setEnabled(true);
						repaint();
					}
				}
				catch(Exception err)
				{
					System.out.println(err);
				}
			} // end method actionPerformed
		} // end anonymous inner class
		);
		buttonJPanel.add(clear_btn);
		clear_btn.addActionListener(new ActionListener() // anonymous inner class
		{
			public void actionPerformed( ActionEvent event )
			{
					try
					{
						shapes.clear();
						line_c.clear();
						fill_c.clear();
						strk.clear();
						re_shapes.clear();
						re_line_c.clear();
						re_fill_c.clear();
						re_strk.clear();
						undo_btn.setEnabled(false);
						redo_btn.setEnabled(false);
						repaint();
					}
					catch(Exception err)
					{
						System.out.println(err);
					}
			} // end method actionPerformed
		} // end anonymous inner class
		);
		buttonJPanel.add(about);
		about.addActionListener(new ActionListener() // anonymous inner class
		{
			public void actionPerformed( ActionEvent event )
			{
					try
					{
						details.author();
					}
					catch(Exception err)
					{
						System.out.println(err);
					}
			} // end method actionPerformed
		} // end anonymous inner class
		);
		label_arc_wd.setVisible(false);
		label_arc_ht.setVisible(false);
		arc_width.setVisible(false);
		arc_height.setVisible(false);
		undo_btn.setEnabled(false);
		redo_btn.setEnabled(false);
		ButtonHandler handler = new ButtonHandler();
		freehand.addActionListener(handler);
		line.addActionListener(handler);
		rect.addActionListener(handler);
		roundrect.addActionListener(handler);
		square.addActionListener(handler);
		ellipse.addActionListener(handler);
		circ.addActionListener(handler);
	}
	class ButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			
			try
			{
				switch(e.getActionCommand())
				{
					case "FreeHand" :
					stype = 0;
					break;
					case "Line" :
					stype = 1;
					break;
					case "Rectangle" :
					stype = 2;
					break;
					case "RoundedRectangle" :
					stype = 3;
					break;
					case "Square" :
					stype = 4;
					break;
					case "Ellipse" :
					stype = 5;
					break;
					case "Circle" :
					stype = 6;
					break;
				}
				switch(stype)
				{
					case 3:
					label_arc_wd.setVisible(true);
					label_arc_ht.setVisible(true);
					arc_width.setVisible(true);
					arc_height.setVisible(true);
					break;
					default:
					label_arc_wd.setVisible(false);
					label_arc_ht.setVisible(false);
					arc_width.setVisible(false);
					arc_height.setVisible(false);
					break;
				}
				label.setText("Selected Type : "+e.getActionCommand());
			}
			catch(Exception err)
			{
				System.out.println(err);
			}
		}
	}
	class PaintArea extends JComponent
	{
		private int pointCount = 0; 
		// count number of points
		// array of 10000 java.awt.Point references
		private Point[] points = new Point[ 10000 ];
		Point startDrag, endDrag;
		Shape newShape;
		public PaintArea()
		{
			this.addMouseListener(new MouseAdapter()
			{
				public void mousePressed(MouseEvent e)
				{
					try
					{
						startDrag = new Point(e.getX(), e.getY());
						endDrag = startDrag;
						undo_btn.setEnabled(true);
						redo_btn.setEnabled(false);
						repaint();
					}
					catch(Exception err)
					{
						System.out.println(err);
					}
				}
				public void mouseReleased(MouseEvent e)
				{
					Shape r;
					switch(stype)
					{
						case 0:
						for ( int i = 0; i < pointCount; i++ )
						{
							if(i==0)
							{
								r = makeLine( points[ i ].x, points[ i ].y, points[ i ].x, points[ i ].y );
								shapes.add(r);
								fill_c.add(line_color);
								line_c.add(line_color);
								strk.add(sh_stroke);
							}
							else
							{
								r = makeLine(points[ i-1 ].x, points[ i-1 ].y ,points[ i ].x, points[ i ].y );
								shapes.add(r);
								fill_c.add(line_color);
								line_c.add(line_color);
								strk.add(sh_stroke);
							}
						}
						pointCount = 0;
						break;
						case 1:
						r = makeLine(startDrag.x, startDrag.y,e.getX(), e.getY());
						shapes.add(r);
						fill_c.add(line_color);
						line_c.add(line_color);
						strk.add(sh_stroke);
						break;
						case 2:
						r = makeRectangle(startDrag.x, startDrag.y,e.getX(), e.getY());
						shapes.add(r);
						fill_c.add(fill_color);
						line_c.add(line_color);
						strk.add(sh_stroke);
						break;
						case 3:
						r = makeRoundRectangle(startDrag.x, startDrag.y,e.getX(), e.getY(), arcWidth, arcHeight);
						shapes.add(r);
						fill_c.add(fill_color);
						line_c.add(line_color);
						strk.add(sh_stroke);
						break;
						case 4:
						r = makeSquare(startDrag.x, startDrag.y,e.getX(), e.getY());
						shapes.add(r);
						fill_c.add(fill_color);
						line_c.add(line_color);
						strk.add(sh_stroke);
						break;
						case 5:
						r = makeEllipse(startDrag.x, startDrag.y,e.getX(), e.getY());
						shapes.add(r);
						fill_c.add(fill_color);
						line_c.add(line_color);
						strk.add(sh_stroke);
						break;
						case 6:
						r = makeCircle(startDrag.x, startDrag.y,e.getX(), e.getY());
						shapes.add(r);
						fill_c.add(fill_color);
						line_c.add(line_color);
						strk.add(sh_stroke);
						break;
					}
					re_shapes.clear();
					re_line_c.clear();
					re_fill_c.clear();
					re_strk.clear();
					startDrag = null;
					endDrag = null;
					repaint();
				}
			} );
			this.addMouseMotionListener(new MouseMotionAdapter()
			{
				public void mouseDragged(MouseEvent e)
				{
					try
					{
						switch(stype)
						{
							case 0:
							if ( pointCount < points.length )
							{
								points[ pointCount ] = e.getPoint(); 
								// find point
								++pointCount; 
								// increment number of points in array
								repaint(); 
								// repaint JFrame
							} // end if
							break;
							default:
							endDrag = new Point(e.getX(), e.getY());
							repaint();
							break;
						}
					}
					catch(Exception err)
					{
						System.out.println(err);
					}
				}
			} );
		}
		public void paint(Graphics g)
		{
			Graphics2D g2 = (Graphics2D)g;
			// turn on antialiasing
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
			// fill background
			g2.setPaint(Color.WHITE);
			g2.fill(new Rectangle(0,0,getSize().width,getSize().height));
			// draw the shapes
			for (Shape s : shapes)
			{
				g2.setStroke(strk.get(shapes.indexOf(s)));
				g2.setPaint(fill_c.get(shapes.indexOf(s)));
				g2.fill(s);
				g2.setPaint(line_c.get(shapes.indexOf(s)));
				g2.draw(s);
			}
			// paint the temporary shape
			if (startDrag != null && endDrag != null)
			{
				g2.setPaint(Color.LIGHT_GRAY);
				g2.setStroke(sh_stroke);
				Shape r;
				switch(stype)
				{
					case 0:
					for ( int i = 1; i < pointCount; i++ )
					{
						r = makeLine( points[ i-1 ].x, points[ i-1 ].y, points[ i ].x, points[ i ].y );
						g2.draw(r);
						g2.fill(r);
					}
					break;
					case 1:
					r = makeLine(startDrag.x, startDrag.y,endDrag.x, endDrag.y);
					g2.draw(r);
					g2.fill(r);
					break;
					case 2:
					r = makeRectangle(startDrag.x, startDrag.y,endDrag.x, endDrag.y);
					g2.draw(r);
					break;
					case 3:
					r = makeRoundRectangle(startDrag.x, startDrag.y,endDrag.x, endDrag.y, arcWidth, arcHeight);
					g2.draw(r);
					break;
					case 4:
					r = makeSquare(startDrag.x, startDrag.y,endDrag.x, endDrag.y);
					g2.draw(r);
					break;
					case 5:
					r = makeEllipse(startDrag.x, startDrag.y,endDrag.x, endDrag.y);
					g2.draw(r);
					break;
					case 6:
					r = makeCircle(startDrag.x, startDrag.y,endDrag.x, endDrag.y);
					g2.draw(r);
					break;
				}
			}
		}
		private Line2D.Float makeLine(int x1, int y1, int x2, int y2)
		{
			return new Line2D.Float(x1, y1, x2, y2);
		}
		private Rectangle2D.Float makeRectangle(int x1, int y1, int x2, int y2)
		{
			int x = Math.min(x1, x2);
			int y = Math.min(y1, y2);
			int width = Math.abs(x1 - x2);
			int height = Math.abs(y1 - y2);
			return new Rectangle2D.Float(x, y, width, height);
		}
		private Rectangle2D.Float makeSquare(int x1, int y1, int x2, int y2)
		{
			int x = x1;
			int y = y1;
			int width = Math.abs(x1 - x2);
			int height = Math.abs(y1 - y2);
			int side = Math.min(width, height);
			if(x1>x2)
			{
				x = x1 - side;
			}
			if(y1>y2)
			{
				y = y1 - side;
			}
			return new Rectangle2D.Float(x, y, side, side);
		}
		private RoundRectangle2D.Float makeRoundRectangle(int x1, int y1, int x2, int y2, int arcWidth, int arcHeight)
		{
			int x = Math.min(x1, x2);
			int y = Math.min(y1, y2);
			int width = Math.abs(x1 - x2);
			int height = Math.abs(y1 - y2);
			return new RoundRectangle2D.Float(x, y, width, height, arcWidth, arcHeight);
		}
		private Ellipse2D.Float makeEllipse(int x1, int y1, int x2, int y2)
		{
			int x = Math.min(x1, x2);
			int y = Math.min(y1, y2);
			int width = Math.abs(x1 - x2);
			int height = Math.abs(y1 - y2);
			return new Ellipse2D.Float(x, y, width, height);
		}
		private Ellipse2D.Float makeCircle(int x1, int y1, int x2, int y2)
		{
			int x = x1;
			int y = y1;
			int width = Math.abs(x1 - x2);
			int height = Math.abs(y1 - y2);
			int rad = Math.min(width, height);
			if(x1>x2)
			{
				x = x1 - rad;
			}
			if(y1>y2)
			{
				y = y1 - rad;
			}
			return new Ellipse2D.Float(x, y, rad, rad);
		}
	}
}