# README #

* MyPaint v1.0

### Description ###
* Paint Program to draw basic shapes (line, rectangle, ellipse, square, circle, etc) and free hand drawing.
* Select arc width and arc height of RoundedRectangle.
* User can select Stroke Size, line color and fill color.
* UNDO, REDO and CLEAR_ALL options are also available.